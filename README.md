# Lab3 -- Unit testing

[![pipeline status](https://gitlab.com/Daniil_Livitin/s23-lab3-code-conventions-and-unit-testing/badges/master/pipeline.svg)](https://gitlab.com/Daniil_Livitin/s23-lab3-code-conventions-and-unit-testing/-/commits/master)
## Homework

You can find tests written for the `ThreadController` in the `src/test/java/com.hw.db/controllers/ThreadControllerTest.java`.

There I used 
* **mocks** - eg.`MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)` ) 
* **stubs** - eg. `threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);`)

## Pipeline

Tests was added to pipeline right after the linter step.
```
test:
    stage: test
    image: maven
    script:
        - chmod +x mvnw
        - ./mvnw test
```
